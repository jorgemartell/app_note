class Note < ApplicationRecord
  validates :body, :title, presence: true

  def self.search_by_title(keywords)
    words = keywords.to_s.strip.split
    words.map! { |word| "title LIKE '#{sanitize_sql_like(word)}%'" }
    
    sql = words.join(" AND ")

    where(sql)
  end
end
